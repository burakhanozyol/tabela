$('.collapse').collapse()
$('.fontselect').on('click',function(){
    fontName = $(this).attr('rel');
    console.log(fontName);
    $('.view-text').css("font-family", fontName);
});
$(document).ready(function() {
    $.ajaxSetup({
        beforeSend:function(xmlHttpRequest){
            if ($("#loadingbar").length === 0) {
                  $("body").append("<div id='loadingbar'></div>")
                  $("#loadingbar").addClass("waiting").append($("<dt/><dd/>"));
                  $("#loadingbar").width((50 + Math.random() * 30) + "%");
            }
                  //show the loading div here
        },
        complete:function(){
                $("#loadingbar").width("101%").delay(200).fadeOut(400, function() {
                       $(this).remove();
                   });
        //remove the div here
        }
    });

});
