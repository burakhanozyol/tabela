//jtwt.js by Harbor (http://jtwt.hrbor.com)

(function($){

 	$.fn.extend({

		//pass the options variable to the function
 		jtwt: function(options) {

			//Set the default values, use comma to separate the settings, example:
			var defaults = {
				username : 'birtanyildiz',
                count : 4,
                image_size: 0,
                convert_links: 1,
                loader_text: 'Loading Tweets...'
			}

			var options =  $.extend(defaults, options);
			var $elem = $(this);

			$elem.html('<p class="jtwt_loader muted" style="">' + options.loader_text + '</p>');

			$.ajax({
				type: "POST",
			   	url: "_assets/ajax/twitter.php",
			   	data: "username=" + options.username,
			   	async:false,
				success: function(response) {
					$elem.html(response);
			   	},
				error: function() {
					$elem.html('<p class="muted">There was a problem loading Tweets at this time.</p>');
				}
			});

		}

		});

})(jQuery);
