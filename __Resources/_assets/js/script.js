var colorSelected = "";
$('.collapse').collapse();
var colorSelectedFront = "", colorSelectedSide = "";

$(".fontselect").on("click", function() {
    fontName = $(this).attr('rel');
    console.log("font adı " + fontName);
    $('.view-text').css('font-family', fontName);
});

$('document').ready(function() {


    $(window).scroll(function(){
      if ($(this).scrollTop() > 185) {
          $('.wells').addClass('fixed');
      } else {
          $('.wells').removeClass('fixed');
      }
  });


    var writeText = $(".write-text");
    $('.texting > p').html(writeText.val());
    writeText.keyup(function() {
        $('.texting > p').html(writeText.val());
    });

    $('.materials').on('click',function(){
        loadMaterial = $(this).attr('rel');
        $( "#ajaxDesing > #accordion" ).load( "variety/" + loadMaterial + "/" + loadMaterial + ".html" );

    });

    $('.colorselect').on('click', function() {
        $(".view-text").removeClass(colorSelected);
        $(".view-text").removeClass(colorSelectedFront);
        colorSelected = $(this).attr("rel");
        colorSelectedFront = $(this).attr("at");
        console.log(colorSelectedFront);
        $(".view-text").addClass(colorSelectedFront);
        $(".view-text").addClass(colorSelected);
    });

    $('.colorselectedside').on('click', function() {
        $(".view-text").removeClass(colorSelected);
        $(".view-text").removeClass(colorSelectedSide);
        colorSelectedSide = $(this).attr("rel");
        console.log(colorSelectedSide);
        $(".view-text").addClass(colorSelectedSide);
    });


});
